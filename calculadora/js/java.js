function iniciar(){

    var resultado = document.getElementById('resultado');
    var borrar = document.getElementById('borrar');
    var suma = document.getElementById('suma');
    var resta = document.getElementById('resta');
    var multiplicasion = document.getElementById('multiplicasion');
    var division = document.getElementById('division');
    var igual = document.getElementById('igual');
    var uno = document.getElementById('uno');
    var dos = document.getElementById('dos');
    var tres = document.getElementById('tres');
    var cuatro = document.getElementById('cuatro');
    var cinco = document.getElementById('cinco');
    var seis = document.getElementById('seis');
    var siete = document.getElementById('siete');
    var ocho = document.getElementById('ocho');
    var nueve = document.getElementById('nueve');
    var cero = document.getElementById('cero');
    var decimal= document.getElementById('decimal');


    var operando_a;
    var operando_b;
    var operacion;


       uno.onclick = function(){
        resultado.textContent = resultado.textContent + "1";
       }
       dos.onclick = function(){
           resultado.textContent = resultado.textContent  + "2";
       }
       tres.onclick = function(){
           resultado.textContent = resultado.textContent  + "3";
       }
       cuatro.onclick = function(){
           resultado.textContent = resultado.textContent  + "4";
       }
       cinco.onclick = function(){
           resultado.textContent = resultado.textContent  + "5";
       }
       seis.onclick = function(){
           resultado.textContent = resultado.textContent  + "6";
       }
       siete.onclick = function(){
           resultado.textContent = resultado.textContent  + "7";
       }
       ocho.onclick = function(){
           resultado.textContent = resultado.textContent  + "8";
       }
       nueve.onclick = function(){
           resultado.textContent = resultado.textContent  + "9";
       }
       cero.onclick = function(){
           resultado.textContent = resultado.textContent  + "0";
       }
       borrar.onclick = function(){
        resetear();
       }

       suma.onclick = function(){
         operando_a = resultado.textContent;
         operacion = "+";
         limpiar();
        }
         resta.onclick = function(){
        operando_a = resultado.textContent;
        operacion = "-";
        limpiar();
    }
    multiplicasion.onclick = function(){
        operando_a = resultado.textContent;
        operacion = "*";
        limpiar();
    }
    division.onclick = function(){
        operando_a = resultado.textContent;
        operacion = "/";
        limpiar();
    }
    igual.onclick = function(){
        operando_b = resultado.textContent;
        resolver();
    }

    function limpiar(){
        resultado.textContent = "";
    }
    function resetear(){
        resultado.textContent = "";
        operando_a = 0;
        operando_b = 0;
        operacion = "";
    }

    function resolver(){
        var res = 0;
        switch(operacion){
          case "+":
            res = parseFloat(operando_a) + parseFloat(operando_b);
            break;
          case "-":
              res = parseFloat(operando_a) - parseFloat(operando_b);
              break;
          case "*":
            res = parseFloat(operando_a) * parseFloat(operando_b);
            break;
          case "/":
            res = parseFloat(operando_a) / parseFloat(operando_b);
            break;
        }
        resetear();
        resultado.textContent = res;
      }

  }